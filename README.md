# IoTWeather #

Internet of Things - Wetterstation
by Thomas Schranz


## Kurzbeschrieb ##

Die App spricht eine RESTFul API an mit der Sensordaten abgerufen werden können.

Die Daten werden in der App in einem Dashboard dargestellt.

Weiter kann eine Messung gestartet werden welche in einem mit in  einstellbaren Intervall die Daten Aufnimmt.Die Messung wird in einem Chart dargestellt.

In den Einstellungen kann die Temperaturskala, Server-URL und Messintervall des Dashboards eingestellt werden.

Da der Server selbst gemacht wurde und primär lokal zur verfügung steht, kann in den Einstellungen ein Schalter auf Dummy-Data gelegt werden. So werden Zufallsdaten generiert um die App zu testen.

## Hardware ##
Der Server läuft auf einem Ultra-low-Power-32-Bit-Mikrocontroller. Der [ESP8266](https://en.wikipedia.org/wiki/ESP8266) ist ein programmierbare WLAN-SoC der Firma espressif.
In diesem Projekt wurde das Board [D1 mini von WeMos](http://www.wemos.cc/Products/d1_mini.html) verwendet.

####Sensoren:#####
* DHT22 (Luftfeuchtigkeit)
* MS5611 (Luftdruck und Temperatur)

#### Software (Server) ####
Zum schreiben der Software für den Server und die Ansteuerung der Sensoren wurde die [Arduino Plattorm](https://www.arduino.cc) verwendet.

## Web-API ##
Die API verarbeitet folgende GET-Requests:

* url/temperature
* url/humidity
* url/pressure
* url/all

## Credits ##
* [Arduino team: ](https://github.com/arduino/Arduino)Die Arduino IDE
* [Ivan Grokhotkov: ](https://github.com/esp8266/Arduino)Package um den ESP6288 mit der Arduino IDE zu programmieren
* [Daniel Cohen Gindi & Philipp Jahoda: ](https://github.com/danielgindi/Charts)Charts Library

## Weiterentwicklung ##
Die App könnte um eine Datenbank der Messungen erweitert werden. So könnte man die aktuelle Messung speichern und vergangene im Chart darstellen. 