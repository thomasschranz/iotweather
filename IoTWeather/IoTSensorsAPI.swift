//
//  IoTSensorsAPI.swift
//  IoTWeather
//
//  Created by Thomas Schranz on 30.05.16.
//  Copyright © 2016 ThomasSchranz. All rights reserved.
//


import Foundation
import UIKit

class IoTSensorsAPI {
    
    let connector:APIConnector = APIConnector()
    let defaults = NSUserDefaults.standardUserDefaults()
    
    // Dialog box
    var refreshAlert = UIAlertController(title: "Server Error", message: "Can't acces Server.\n Change Server-URL in Settings. \n\nAs for now, dummy data will be displayed.", preferredStyle: UIAlertControllerStyle.Alert)
    
    
    func castToSensorData(representation:[String: AnyObject]) -> SensorData {
    
        // Value if Measurement failed (server not responding correctly)
        var temp = -500.0
        var hum = temp
        var pre = temp
        
        if representation["temperature"] is Double{
            temp =  representation["temperature"] as! Double
        }
        
        if representation["humidity"] is Double{
            hum =  representation["humidity"] as! Double
        }
        
        if representation["pressure"] is Double{
            pre =  representation["pressure"] as! Double
        }
        return SensorData(temperature: temp, humidity: hum, pressure: pre, timeStamp: NSDate())
    }
    
    /** Get the latest sensor-values */
    func getSensorValues(completion: SensorData -> Void) -> Void {
        // Dummy Measurement
        if defaults.boolForKey("dummyData") {
            let rT = drand48()*2 + 20
            let rH = drand48()*6 + 50
            let rP = drand48()*10 + 95200
        
            let data = SensorData(temperature: rT,humidity: rH,pressure: rP,timeStamp: NSDate())
            completion(data)
        }
        else{
            // http://iotsensor.ddns.net/all
            // http://212.60.43.246:128/all
            connector.performRequest(self.defaults.stringForKey("serverURL")! as String, method: "GET", body: nil){ result in
                switch(result) {
                case .Success(let responseObject):
                    let rawSensorData = responseObject as! [String: AnyObject]
                    let data = self.castToSensorData(rawSensorData)
                    completion(data)
                case .Failure( _):
                    let errData = SensorData(temperature: -500.0, humidity: -500.0, pressure: -500.0, timeStamp: NSDate())
                    //print(errorMessage)
                    completion(errData)
                case .NetworkError( _):
                    let errData = SensorData(temperature: -500.0, humidity: -500.0, pressure: -500.0, timeStamp: NSDate())
                    //print(error)
                    completion(errData)
                }
            }
        }
    }
    
    /** Get the data for the Dashboard as formatted strings */
    func getDatasForDashboard(completion: SensorDataString -> Void) -> Void {
        self.getSensorValues() { data in
            if data.temperature == -500 || data.humidity == -500 || data.pressure == -500 {
                let nodataString = SensorDataString(
                    temperature: "no Data",
                    humidity: "no Data",
                    pressure: "no Data",
                    timeStamp: "no Data")
                completion(nodataString)
                
            } else{
                let dateFormatter = NSDateFormatter()
                dateFormatter.locale = NSLocale.currentLocale()
                dateFormatter.timeStyle = NSDateFormatterStyle.MediumStyle
                
                let timeString = dateFormatter.stringFromDate(data.timeStamp)
                
                var tempString:String
                if(self.defaults.integerForKey("tempScale") == 1){
                    tempString = String(format:"%.2f", data.temperature) + " °C"
                }else{
                    tempString = String(format:"%.2f", 9/5 * data.temperature + 32.0) + " °F"
                }
                
                let humString = String(format:"%.2f", data.humidity) + " %"
                let preString = String(format:"%.0f", data.pressure) + " Pa"
                
                let dataString = SensorDataString(
                    temperature: tempString,
                    humidity: humString,
                    pressure: preString,
                    timeStamp: timeString)
                completion(dataString)
            }
        }
    }
    
}