//
//  FirstViewController.swift
//  IoTWeather
//
//  Created by Thomas Schranz on 10.05.16.
//  Copyright © 2016 ThomasSchranz. All rights reserved.
// 

import UIKit

class DashboardViewController: UIViewController {

    
    @IBOutlet weak var lblTemperature: UILabel!
    @IBOutlet weak var lblHumidity: UILabel!
    @IBOutlet weak var lblPressure: UILabel!
    @IBOutlet weak var lblUpdate: UILabel!
    
    
    var timerRunning = false
    var timer = NSTimer()
    
    let sensorAPI = IoTSensorsAPI()
    
    let defaults = NSUserDefaults.standardUserDefaults()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sensorAPI.refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
        }))
        
    }
    
    override func viewWillAppear(animated: Bool) {
        if !timerRunning {
            updateDashboard()
            timer = NSTimer.scheduledTimerWithTimeInterval(10, target: self, selector: #selector(updateDashboard), userInfo: nil, repeats: true)
            timerRunning = true
        }
    }
    override func viewDidDisappear(animated: Bool) {
        if timerRunning {
            timer.invalidate()
            timerRunning = false
        }
    }
    
    func updateDashboard(){
        sensorAPI.getDatasForDashboard() { data in
    
            dispatch_async(dispatch_get_main_queue()) {
                self.lblUpdate.text      = data.timeStamp
                self.lblTemperature.text = data.temperature
                self.lblHumidity.text    = data.humidity
                self.lblPressure.text    = data.pressure
                
                if data.temperature == "no Data" ||  data.humidity == "no Data" || data.pressure == "no Data"{
                    self.defaults.setBool(true, forKey: "dummyData")
                    self.presentViewController(self.sensorAPI.refreshAlert, animated: true, completion: nil)
                }
            }
        }
    }

    
}

