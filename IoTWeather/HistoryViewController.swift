//
//  SecondViewController.swift
//  IoTWeather
//
//  Created by Thomas Schranz on 10.05.16.
//  Copyright © 2016 ThomasSchranz. All rights reserved.
//

import UIKit
import Charts

class HistoryViewController: UIViewController, ChartViewDelegate {
    
    // Outlets
    @IBOutlet weak var btnLoad: UIButton!
    @IBOutlet weak var btnStart: UIButton!
    @IBOutlet weak var chartView: LineChartView!
    @IBOutlet weak var colorBarTemperature: UIView!
    @IBOutlet weak var colorBarPressure: UIView!
    @IBOutlet weak var colorBarHumidity: UIView!
    
    // Temperature Constants
    final let TemperatureColor = UIColor(red: 255/255, green: 102/255, blue: 102/255, alpha: 1.0) /* #ff6666 */
    final let HumidityColor = UIColor(red: 102/255, green: 204/255, blue: 255/255, alpha: 1.0) /* #66ccff */
    final let PressureColor = UIColor(red: 204/255, green: 255/255, blue: 102/255, alpha: 1.0) /* #ccff66 */
    final let InvisibleColor = UIColor.grayColor()
    final let measurementActiveColor = UIColor.redColor()
    final let measurementInactiveColor = UIColor(red: 0/255, green: 128/255, blue: 0/255, alpha: 1.0) /* #008000 */
    
    // Which datasets are diplayed
    var temperatureIsVisible = true
    var humidityIsVisible = true
    var pressureIsVisible = true
    
    var newMeasurement = true
    var measurementActive = false
    
    var timer = NSTimer()
    
    let defaults = NSUserDefaults.standardUserDefaults()
    
    let sensorAPI = IoTSensorsAPI()
    
    var myDataStore = DataStore()

    override func viewDidLoad() {
        super.viewDidLoad()
        chartView.delegate = self
        initChart()
        
        sensorAPI.refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
        }))
        
    }
    
    /** Enable/Diable dataset Temperature */
    @IBAction func btnTemperature(sender: AnyObject) {
        temperatureIsVisible = !temperatureIsVisible
        if temperatureIsVisible {
            colorBarTemperature.backgroundColor = TemperatureColor
            dataSetTemperature.visible = true
        }else{
            colorBarTemperature.backgroundColor = InvisibleColor
            dataSetTemperature.visible = false
        }
        updateYAxis()
        chartView.notifyDataSetChanged()
    }
    /** Enable/Diable dataset Pressure */
    @IBAction func btnPressure(sender: AnyObject) {
        pressureIsVisible = !pressureIsVisible
        if pressureIsVisible {
            colorBarPressure.backgroundColor = PressureColor
            dataSetPressure.visible = true
        }else{
            colorBarPressure.backgroundColor = InvisibleColor
            dataSetPressure.visible = false
        }
        updateYAxis()
        chartView.notifyDataSetChanged()
    }
    /** Enable/Diable dataset Humidity */
    @IBAction func btnHumidity(sender: AnyObject) {
        humidityIsVisible = !humidityIsVisible
        if humidityIsVisible {
            colorBarHumidity.backgroundColor = HumidityColor
            dataSetHumidity.visible = true
        }else{
            colorBarHumidity.backgroundColor = InvisibleColor
            dataSetHumidity.visible = false
        }
        updateYAxis()
        chartView.notifyDataSetChanged()
    }
    
    
    @IBAction func btnLoad(sender: AnyObject) {
        myDataStore.clear()
        chartView.clear()
        newMeasurement = true
        btnStart.setTitle("Start", forState: UIControlState.Normal)
    }

    /** Starts/Stops measurement */
    @IBAction func btnStart(sender: AnyObject) {
        measurementActive = !measurementActive
        
        if measurementActive {
            btnLoad.enabled = false
            // Start measurement
            btnStart.setTitleColor(measurementActiveColor, forState: UIControlState.Normal)
            btnStart.setTitle("Stop", forState: UIControlState.Normal)
            
            if newMeasurement {
                initDataSetTemperature()
                initDataSetHumidity()
                initDataSetPressure()
                initChartData()
            }
            
            measure()
            timer = NSTimer.scheduledTimerWithTimeInterval((Double)(defaults.integerForKey("measIntervall")), target: self, selector: #selector(measure), userInfo: nil, repeats: true)
            measurementActive = true
            
        }else{
            btnLoad.enabled = true
            // Stop measurement
            btnStart.setTitleColor(measurementInactiveColor, forState: UIControlState.Normal)
            btnStart.setTitle("Continue", forState: UIControlState.Normal)
            timer.invalidate()
            newMeasurement = false
       }
    }
    
    /** Get Values from the Sensor */
    func measure() -> Void {
        
        sensorAPI.getSensorValues(){ dataPoint in
            if dataPoint.temperature == -500 ||  dataPoint.humidity == -500 || dataPoint.pressure == -500{
                dispatch_async(dispatch_get_main_queue(), {
                    self.defaults.setBool(true, forKey: "dummyData")
                    self.presentViewController(self.sensorAPI.refreshAlert, animated: true, completion: nil)
                })
                
            }else{
                self.myDataStore.setData(dataPoint)
                dispatch_async(dispatch_get_main_queue()) {
                    self.updateChartLive()
                }
            }
        }
    }
    
    /** Update Chart when one new value was added.*/
    func updateChartLive() {
        // X-Value (time)
        chartView.data?.addXValue(myDataStore.getLastTimeLegend())
        
        // Y-Value (data)
        let val = myDataStore.getLastData()
        
        var dataEntry = ChartDataEntry(value: val.temperature, xIndex: myDataStore.count()-1)
        self.dataSetTemperature.addEntry(dataEntry)
        
        dataEntry = ChartDataEntry(value: val.humidity, xIndex: myDataStore.count()-1)
        self.dataSetHumidity.addEntry(dataEntry)
        
        dataEntry = ChartDataEntry(value: val.pressure, xIndex: myDataStore.count()-1)
        self.dataSetPressure.addEntry(dataEntry)
        
        // Y-Axis
        updateYAxis()
    
        chartView.notifyDataSetChanged()
    }
    
    /** Sets limits of the Y-Axis, depending on the datasets and values displayed */
    func setYAxisLimits(axis : String, dataSet : String){
        var max = 0.0
        var min = 0.0
        var diff = 0.0
        switch dataSet {
        case "t":
            max = myDataStore.maxTemperature
            min = myDataStore.minTemperature
            diff = myDataStore.diffTemperature
        case "h":
            max = myDataStore.maxHumidity
            min = myDataStore.minHumidity
            diff = myDataStore.diffHumidity
        case "p":
            max = myDataStore.maxPressure
            min = myDataStore.minPressure
            diff = myDataStore.diffPressure
        default:
            break
        }
        
        if axis == "left" {
            chartView.leftAxis.axisMaxValue = max + diff
            chartView.leftAxis.axisMinValue = min - diff
        }else if axis == "right"{
            chartView.rightAxis.axisMaxValue = max + diff
            chartView.rightAxis.axisMinValue = min - diff
        }
    }
    
    /** Sets the axisDependency of the Y-Axis, depending on the datasets displayed */
    func updateYAxis() {
        // How many active
        var numActive = 0
        if temperatureIsVisible { numActive += 1}
        if humidityIsVisible { numActive += 1}
        if pressureIsVisible { numActive += 1}
        
        // 0 or active
        if numActive <= 1 {
            if humidityIsVisible {
                setYAxisLimits("left", dataSet: "h")
            }else if pressureIsVisible{
                setYAxisLimits("left", dataSet: "p")
            }else{
                setYAxisLimits("left", dataSet: "t")
            }
            chartView.rightAxis.axisMaxValue = chartView.leftAxis.axisMaxValue
            chartView.rightAxis.axisMinValue = chartView.leftAxis.axisMinValue
            
            dataSetTemperature.axisDependency = ChartYAxis.AxisDependency.Left
            dataSetHumidity.axisDependency = ChartYAxis.AxisDependency.Left
            dataSetPressure.axisDependency = ChartYAxis.AxisDependency.Left

        }
        
        // 2 active
        if numActive == 2 {
            if temperatureIsVisible{
                setYAxisLimits("left", dataSet: "t")
                dataSetTemperature.axisDependency = ChartYAxis.AxisDependency.Left
                if humidityIsVisible {
                    dataSetHumidity.axisDependency = ChartYAxis.AxisDependency.Right
                    setYAxisLimits("right", dataSet: "h")
                }else {
                    dataSetPressure.axisDependency = ChartYAxis.AxisDependency.Right
                    setYAxisLimits("right", dataSet: "p")
                }
            }else{
                dataSetHumidity.axisDependency = ChartYAxis.AxisDependency.Left
                dataSetPressure.axisDependency = ChartYAxis.AxisDependency.Right
                setYAxisLimits("left", dataSet: "h")
                setYAxisLimits("right", dataSet: "p")
            }
        }
        
        // 3 active
        if numActive == 3 {
            dataSetTemperature.axisDependency = ChartYAxis.AxisDependency.Left
            dataSetHumidity.axisDependency = ChartYAxis.AxisDependency.Left
            dataSetPressure.axisDependency = ChartYAxis.AxisDependency.Right
            
            if myDataStore.maxTemperature > myDataStore.maxHumidity {
                chartView.leftAxis.axisMaxValue = myDataStore.maxTemperature + myDataStore.diffTemperature
            }else{
                chartView.leftAxis.axisMaxValue = myDataStore.maxHumidity + myDataStore.diffHumidity
            }
            if myDataStore.minTemperature < myDataStore.minHumidity {
                chartView.leftAxis.axisMinValue = myDataStore.minTemperature - myDataStore.diffTemperature
            }else{
                chartView.leftAxis.axisMinValue = myDataStore.minHumidity - myDataStore.diffHumidity
            }
            
            setYAxisLimits("right", dataSet: "p")
        }
    }

    
    /** Initialize Chart with data */
    func initChartData() {
        
        // Add dummy value
        var dataEntry = ChartDataEntry(value: 20, xIndex: 0)
        self.dataSetTemperature.addEntry(dataEntry)
    
        dataEntry = ChartDataEntry(value: 50, xIndex: 0)
        self.dataSetHumidity.addEntry(dataEntry)
        
        dataEntry = ChartDataEntry(value: 1000, xIndex: 0)
        self.dataSetPressure.addEntry(dataEntry)
        
        // create an array to store LineChartDataSets
        var dataSets : [LineChartDataSet] = [LineChartDataSet]()
        dataSets.append(dataSetHumidity)
        dataSets.append(dataSetTemperature)
        dataSets.append(dataSetPressure)
        
        let data: LineChartData = LineChartData(xVals: [""], dataSets: dataSets)
        
        self.chartView.data = data
        chartView.notifyDataSetChanged()

        // remove dummy value
        dataSetTemperature.removeLast()
        dataSetHumidity.removeLast()
        dataSetPressure.removeLast()
        self.chartView.data?.removeXValue(0)
        
     
    }
    
    var dataSetTemperature : LineChartDataSet!
    var dataSetHumidity : LineChartDataSet!
    var dataSetPressure : LineChartDataSet!
    
    func initDataSetTemperature(){
        let dataSet = LineChartDataSet(yVals: [], label: "Temperature")
        dataSet.colors = [TemperatureColor]
        dataSet.circleColors = dataSet.colors
        dataSet.circleRadius = 3
        dataSet.lineWidth = 2
        dataSet.highlightEnabled = false
        dataSet.drawValuesEnabled = false
        dataSet.visible = true
        dataSet.drawCircleHoleEnabled = false
        dataSet.axisDependency = ChartYAxis.AxisDependency.Left
        dataSetTemperature = dataSet
    }
    
    func initDataSetHumidity(){
        let dataSet = LineChartDataSet(yVals: [], label: "Humidity")
        dataSet.colors = [HumidityColor]
        dataSet.circleColors = dataSet.colors
        dataSet.circleRadius = 3
        dataSet.lineWidth = 2
        dataSet.highlightEnabled = false
        dataSet.drawValuesEnabled = false
        dataSet.visible = true
        dataSet.drawCircleHoleEnabled = false
        dataSet.axisDependency = ChartYAxis.AxisDependency.Left
        dataSetHumidity = dataSet
    }
    
    func initDataSetPressure(){
        let dataSet = LineChartDataSet(yVals: [], label: "Pressure")
        dataSet.colors = [PressureColor]
        dataSet.circleColors = dataSet.colors
        dataSet.circleRadius = 3
        dataSet.lineWidth = 2
        dataSet.highlightEnabled = false
        dataSet.drawValuesEnabled = false
        dataSet.visible = true
        dataSet.drawCircleHoleEnabled = false
        dataSet.axisDependency = ChartYAxis.AxisDependency.Right
        dataSetPressure = dataSet
    }
    
    func initChart() {

        chartView.noDataText = "Start recording..." //  or load measured data from the database
        chartView.descriptionText = ""
        
        chartView.pinchZoomEnabled = true
        chartView.scaleYEnabled = false
        chartView.dragEnabled = true
        chartView.legend.enabled = false
        
        chartView.doubleTapToZoomEnabled = false
        chartView.drawGridBackgroundEnabled = false
        
        chartView.xAxis.spaceBetweenLabels = 5
    }

}

