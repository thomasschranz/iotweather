//
//  SecondViewController.swift
//  IoTWeather
//
//  Created by Thomas Schranz on 10.05.16.
//  Copyright © 2016 ThomasSchranz. All rights reserved.
//

import UIKit

class SettingsViewController: UITableViewController, UITextFieldDelegate{
    
    @IBOutlet weak var lblIntervalSeconds: UILabel!
    @IBOutlet weak var selTempScale: UISegmentedControl!
    @IBOutlet weak var swDummy: UISwitch!
    @IBOutlet weak var txtServerURL: UITextField!
    @IBOutlet weak var stpMeasIntervall: UIStepper!
    
    let defaults = NSUserDefaults.standardUserDefaults()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        updateElements()
        
        self.txtServerURL.delegate = self
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        updateElements()
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        defaults.synchronize()
    }
    
    func updateElements() {
        selTempScale.selectedSegmentIndex = defaults.integerForKey("tempScale")
        lblIntervalSeconds.text = defaults.integerForKey("measIntervall").description
        stpMeasIntervall.value = (Double)(defaults.integerForKey("measIntervall"))
        
        txtServerURL.text = (self.defaults.objectForKey("serverURL")! as! String)
        
        swDummy.setOn(defaults.boolForKey("dummyData"), animated: false)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.view.endEditing(true)
        defaults.setObject(textField.description, forKey: "serverURL")
        return false
    }
    
    @IBAction func selTempScale(sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex
        {
        case 0:
            defaults.setInteger(0, forKey: "tempScale")
        case 1:
            defaults.setInteger(1, forKey: "tempScale")
        default:
            break; 
        }
    }
    
    @IBAction func stpIntervalChanger(sender: UIStepper) {
        defaults.setInteger((Int)(sender.value), forKey: "measIntervall")
        lblIntervalSeconds.text = defaults.integerForKey("measIntervall").description
        
    }
    
    @IBAction func txtServerURL(sender: UITextField) {
        let url:NSString = sender.text!
        
        defaults.setObject(url, forKey: "serverURL")
    }
    
    @IBAction func swDummy(sender: UISwitch) {
        defaults.setBool(sender.on, forKey: "dummyData")
    }
}

