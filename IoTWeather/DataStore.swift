//
//  DataStore.swift
//  IoTWeather
//
//  Created by Thomas Schranz on 14.06.16.
//  Copyright © 2016 ThomasSchranz. All rights reserved.
//

import Foundation

class DataStore {

    private var datas = [SensorData]()
    private var timeLegend = [Int]()
    private var xValsLegend = [String]()
    
    var maxTemperature = -100.0
    var maxHumidity = -1.0
    var maxPressure = -1.0
    var minTemperature = 100.0
    var minHumidity = 101.0
    var minPressure = 1101325.0
    
    let diffTemperature = 1.0
    let diffHumidity = 1.0
    let diffPressure = 5.0
    
    func clear() -> Void {
        datas = [SensorData]()
    }
    
    func getAllDatas() -> [SensorData] {
        return datas
    }
    
    func setAllData(dataS: [SensorData]) {
        datas = dataS
        
        // Find max/min Values
        for dataP in dataS {
            testSetMaxMin(dataP)
        }

    }
    
    /** Set one Data Point */
    func setData(dataP : SensorData) {
        datas.append(dataP)
        testSetMaxMin(dataP)
    }
    
    func getLastData() -> SensorData {
        return datas[datas.count-1]
    }
    
    /** Calculate x-Index for the Chart */
    func calcXIndex(dataS: [SensorData]) -> [Int] {
        // x Value every Second
        var xVals = [0]
        
        if(dataS.count <= 1){
            return [0]
        }
        
        for var index = 0; index < dataS.count-1 ; index = index+1 {
            let elapsedTime = dataS[index+1].timeStamp.timeIntervalSinceDate(dataS[index].timeStamp)
            let duration = Int(elapsedTime)
            
            // last value + difference
            xVals.append(xVals[index] + duration)
        }
        return xVals
    }

    
    /** Return x-Legend for the Chart */
    private func timeLegendFormat(dataS: [SensorData]) -> [String] {

        let dateFormatter = NSDateFormatter()
        dateFormatter.locale = NSLocale.currentLocale()
        
        // Todo: Custom xValue Labels ( every hour -> write HH, every new Day --> write Day...)
        dateFormatter.dateFormat = "HH:MM:ss"
        
        var timeStampString = [String](count: dataS.count, repeatedValue: "")
        
        for (index,_) in dataS.enumerate() {
            timeStampString[index] = dateFormatter.stringFromDate(dataS[index].timeStamp)
        }
        
        return timeStampString
    }
    
    
    func getLastxVal() -> [Int] {
        let numOfDatas = datas.count
        if (numOfDatas <= 1){
            return [0]
        }
        let lastTwoValues = [datas[numOfDatas-2],datas[numOfDatas-1]]
    
        return calcXIndex(lastTwoValues)
    }
    
    func getLastTimeLegend() -> String {
        let numOfDatas = datas.count
        if (numOfDatas < 1){
            return ""
        }
        
        if (numOfDatas == 1){
            let lastData = datas[0]
            return timeLegendFormat([lastData])[0]
        }
        
        let lastData = datas[numOfDatas-1]
        return timeLegendFormat([lastData])[0]
    }
    
    func count() -> Int {
        return self.datas.count
    }
    

    private func testSetMaxMin(dataP : SensorData) {
        // Max
        if dataP.temperature > maxTemperature {
            maxTemperature = dataP.temperature
        }
        if dataP.humidity > maxHumidity {
            maxHumidity = dataP.humidity
        }
        if dataP.pressure > maxPressure {
            maxPressure = dataP.pressure
        }
        // Min
        if dataP.temperature < minTemperature {
            minTemperature = dataP.temperature
        }
        if dataP.humidity < minHumidity {
            minHumidity = dataP.humidity
        }
        if dataP.pressure < minPressure {
            minPressure = dataP.pressure
        }
    }
    
}