//
//  SensorData.swift
//  IoTWeather
//
//  Created by Thomas Schranz on 31.05.16.
//  Copyright © 2016 ThomasSchranz. All rights reserved.
//

import Foundation

struct SensorData {
    let temperature:Double
    let humidity:Double
    let pressure:Double
    let timeStamp:NSDate

}


struct SensorDataString {
    let temperature:String
    let humidity:String
    let pressure:String
    let timeStamp:String
    
}